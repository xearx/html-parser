class Link {
  constructor() {
    this.url = '';
    this.text = '';
    this.title = '';
  }

  /**
   * @param {string} url
   */
  setUrl(url) {
    this.url = url;
    return this;
  }

  /**
   * @param {sing} text
   */
  setText(text) {
    this.text = text;
    return this;
  }

  /**
   * @param {string} title
   */
  setTitle(title) {
    this.title = title;
    return this;
  }
}

module.exports = Link;
