const Page = require('./page');
const Link = require('./link');

describe('Page', () => {
  describe('Serialization', () => {
    it('.toJSON() should serialize a page object', () => {
      const page = new Page()
        .setTitle('How to eat an apple')
        .addHeadings([
          'Generate Dummy Text',
          'Results',
        ])
        .addParagraphs([
          'Dapibus. Vitae primis sed platea euismod Torquent.',
          'Diam imperdiet. Nascetur ultricies et ipsum primis felis lacus. Diam elit torquent consectetuer sed Suscipit bibendum, blandit natoque felis aptent semper velit mauris phasellus et hendrerit litora nascetur vulputate fringilla. Potenti dictumst tincidunt tempus luctus nibh in.',
          'Netus tellus vulputate suscipit class class integer sem, ante sodales dignissim malesuada in aptent facilisi dictumst eu amet viverra posuere sed id.',
        ])
        .addLinks([
          new Link().setText('jsonify').setUrl('https://github.com/substack/jsonify').setTitle('Hac montes suspendisse'),
          new Link().setText('Alternatives of JSON.stringify() in JavaScript - Stack ...').setUrl('https://stackoverflow.com/questions/1480393/alternatives-of-json-stringify-in-javascript'),
          new Link().setText('You can find the page it originated from here').setUrl('https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify').setTitle('Sodales donec nullam'),
        ]);

      expect(JSON.parse(page.toJSON())).toEqual({
        links: [
          {
            text: 'jsonify',
            url: 'https://github.com/substack/jsonify',
            title: 'Hac montes suspendisse',
          },
          {
            text: 'Alternatives of JSON.stringify() in JavaScript - Stack ...',
            url: 'https://stackoverflow.com/questions/1480393/alternatives-of-json-stringify-in-javascript',
            title: '',
          },
          {
            text: 'You can find the page it originated from here',
            url: 'https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON/stringify',
            title: 'Sodales donec nullam',
          },
        ],
        paragraphs: [
          'Dapibus. Vitae primis sed platea euismod Torquent.',
          'Diam imperdiet. Nascetur ultricies et ipsum primis felis lacus. Diam elit torquent consectetuer sed Suscipit bibendum, blandit natoque felis aptent semper velit mauris phasellus et hendrerit litora nascetur vulputate fringilla. Potenti dictumst tincidunt tempus luctus nibh in.',
          'Netus tellus vulputate suscipit class class integer sem, ante sodales dignissim malesuada in aptent facilisi dictumst eu amet viverra posuere sed id.',
        ],
        headings: [
          'Generate Dummy Text',
          'Results',
        ],
        title: 'How to eat an apple',
      });
    });
  });
});
