const stringify = require('fast-json-stable-stringify');

class Page {
  constructor() {
    this.links = [];
    this.paragraphs = [];
    this.headings = [];
  }


  /**
   * Sets page title.
   *
   * @param {string} title
   * @returns {Page} page itself for chaining.
   */
  setTitle(title) {
    this.title = title;
    return this;
  }

  /**
   * Appends a link to page links.
   *
   * @param {import('./link')} link
   * @returns {Page} page itself for chaining.
   */
  addLink(link) {
    this.links.push(link);
    return this;
  }

  /**
   * Appends array of links to page links.
   *
   * @param {import('./link')[]} links
   * @returns {Page} page itself for chaining.
   */
  addLinks(links) {
    links.forEach((link) => this.addLink(link));
    return this;
  }

  /**
   * Appends a paragraph to page paragraphs list.
   *
   * @param {string} paragraph
   * @returns {Page} page itself for chaining.
   */
  addParagraph(paragraph) {
    this.paragraphs.push(paragraph);
    return this;
  }

  /**
   * Appends array of paragraphs to page paragraphs list.
   *
   * @param {string[]} paragraphs
   * @returns {Page} page itself for chaining.
   */
  addParagraphs(paragraphs) {
    paragraphs.forEach((paragraph) => this.addParagraph(paragraph));
    return this;
  }

  /**
   * Appends a heading to page headings list.
   *
   * @param {string} heading
   * @returns {Page} page itself for chaining.
   */
  addHeading(heading) {
    this.headings.push(heading);
    return this;
  }

  /**
   * Appends array of headings to page headings list.
   *
   * @param {string[]} headings
   * @returns {Page} page itself for chaining.
   */
  addHeadings(headings) {
    headings.forEach((heading) => this.addHeading(heading));
    return this;
  }

  /**
   * Serializes page to json string.
   *
   * @returns {string} jsonified version of page.
   */
  toJSON() {
    return stringify({
      links: this.links,
      paragraphs: this.paragraphs,
      headings: this.headings,
      title: this.title,
    });
  }
}

module.exports = Page;
