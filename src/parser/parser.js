const { range } = require('lodash');
const cheerio = require('cheerio');
const Page = require('../models/page');
const Link = require('../models/link');


const textExtractor = (element) => element.contents().text();

const titleSelectorsInOrder = new Map([
  ['title', ($) => textExtractor($)],
  ['meta[property="og:title"]', ($) => $.attr('content') ?? ''],
  ['h1', ($) => textExtractor($)],
  ['h2', ($) => textExtractor($)],
  ['h3', ($) => textExtractor($)],
  ['h4', ($) => textExtractor($)],
  ['h5', ($) => textExtractor($)],
  ['h6', ($) => textExtractor($)],
]);

function extractLinks($) {
  return Array.from(
    $('a[href]')
      .filter((_, anchor) => {
        if (anchor.children.length === 0) {
          return false;
        }
        const textElement = anchor.children.find((node) => node.type === 'text' && node.data.trim().length > 0);
        if (!textElement) {
          return false;
        }

        return true;
      })
      .map((_, anchor) => {
        const { href } = anchor.attribs;
        const text = anchor.children.find((node) => node.type === 'text').data.trim();
        const title = anchor.attribs.title ?? '';
        return new Link()
          .setUrl(href)
          .setText(text)
          .setTitle(title);
      }),
  );
}


function extractTitle($) {
  for (const [titleSelector, getter] of titleSelectorsInOrder.entries()) {
    const selectionText = getter($(titleSelector));
    if (selectionText.length > 0) {
      return selectionText;
    }
  }
  return '';
}

function extractElementText(element) {
  if (element.type === 'tag') {
    return element.children.map(extractElementText).join('');
  }
  return element.data;
}

function extractHeadings($) {
  return range(1, 7)
    .map((i) => `h${i}`)
    .map((headingSelector) => Array.from(
      $(headingSelector)
        .map((_, heading) => heading
          .children
          .map(extractElementText)
          .join('')
          .trim()),
    ))
    .flat();
}

function extractParagraphs($) {
  return Array.from(
    $('p')
      .map((_, p) => p
        .children
        .map(extractElementText)
        .join('')
        .trim()),
  );
}

/**
 * @param {string | Buffer} htmlString
 * @returns {import('../models/page')}
 */
function parse(htmlString) {
  const $ = cheerio.load(htmlString);

  const title = extractTitle($);
  const links = extractLinks($);
  const headings = extractHeadings($);
  const paragraphs = extractParagraphs($);

  return new Page()
    .setTitle(title)
    .addLinks(links)
    .addHeadings(headings)
    .addParagraphs(paragraphs);
}

module.exports = {
  parse,
};
