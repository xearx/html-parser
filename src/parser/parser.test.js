const { parse } = require('./parser');
const Link = require('../models/link');

describe('Testing html parser', () => {
  describe('Title extraction', () => {
    it('should extract title from <title>', () => {
      const fakePage = `
    <html>
    <head>
    <meta charSet="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>Getting Started · Jest</title>
    <meta name="viewport" content="width=device-width"/>
    <meta name="generator" content="Docusaurus"/>
    <meta name="description" content="Install Jest using"/>
    <meta name="docsearch:version" content="26.0"/>
    <meta name="docsearch:language" content="en"/>
    <meta property="og:title" content="Getting Started · Jest In Meta"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="https://jestjs.io/"/>
    <meta property="og:description" content="Install Jest using yarn"/>
    </head>
    </html>
    `;
      const page = parse(fakePage);

      expect(page.title).toBe('Getting Started · Jest');
    });

    it('should extract title from <meta og:title=""/> without <title>', () => {
      const fakePage = `
      <html>
      <head>
      <meta charSet="utf-8"/>
      <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
      <meta name="viewport" content="width=device-width"/>
      <meta name="generator" content="Docusaurus"/>
      <meta name="description" content="Install Jest using"/>
      <meta name="docsearch:version" content="26.0"/>
      <meta name="docsearch:language" content="en"/>
      <meta property="og:title" content="Getting Started · Jest"/>
      <meta property="og:type" content="website"/>
      <meta property="og:url" content="https://jestjs.io/"/>
      <meta property="og:description" content="Install Jest using yarn"/>
      </head>
      </html>
      `;
      const page = parse(fakePage);

      expect(page.title).toBe('Getting Started · Jest');
    });

    it('should extract title from <h1></h1>', () => {
      const fakePage = `
      <html>
      <body>
        <h1>Imagine it would be a title!</h1>
      </body>
      </html>
      `;
      const page = parse(fakePage);

      expect(page.title).toBe('Imagine it would be a title!');
    });

    it('should extract title from <h2></h2>', () => {
      const fakePage = `
      <html>
      <body>
        <h2>Imagine it would be a title!</h2>
      </body>
      </html>
      `;
      const page = parse(fakePage);

      expect(page.title).toBe('Imagine it would be a title!');
    });

    it('should extract title from <h3></h3>', () => {
      const fakePage = `
      <html>
      <body>
        <h3>Imagine it would be a title!</h3>
      </body>
      </html>
      `;
      const page = parse(fakePage);

      expect(page.title).toBe('Imagine it would be a title!');
    });

    it('should extract title from <h4></h4>', () => {
      const fakePage = `
      <html>
      <body>
        <h4>Imagine it would be a title!</h4>
      </body>
      </html>
      `;
      const page = parse(fakePage);

      expect(page.title).toBe('Imagine it would be a title!');
    });

    it('should extract title from <h5></h5>', () => {
      const fakePage = `
      <html>
      <body>
        <h5>Imagine it would be a title!</h5>
      </body>
      </html>
      `;
      const page = parse(fakePage);

      expect(page.title).toBe('Imagine it would be a title!');
    });

    it('should extract title from <h6></h6>', () => {
      const fakePage = `
      <html>
      <body>
        <h6>Imagine it would be a title!</h6>
      </body>
      </html>
      `;
      const page = parse(fakePage);

      expect(page.title).toBe('Imagine it would be a title!');
    });

    it('should return an empty string when no shitty title exists', () => {
      const fakePage = `
      <html>
      <body>
        <p>This is a start for a title</p>
      </body>
      </html>
      `;
      const page = parse(fakePage);

      expect(page.title).toBe('');
    });
  });

  describe('Link extraction', () => {
    it('should extract one link from <a> with its text', () => {
      const fakePage = `
    <html>
    <body>
    <a href="/to/some/page">A link to some page</a>
    </body>
    </html>
    `;
      const page = parse(fakePage);

      expect(page.links).toHaveLength(1);
      expect(page.links).toContainEqual(
        new Link()
          .setText('A link to some page')
          .setUrl('/to/some/page'),
      );
    });

    it('should extract anchor title if exists', () => {
      const fakePage = `
    <html>
    <body>
    <a href="/to/some/page" title="a direct link to some page">A link to some page</a>
    </body>
    </html>
    `;
      const page = parse(fakePage);

      expect(page.links).toHaveLength(1);
      expect(page.links).toContainEqual(
        new Link()
          .setText('A link to some page')
          .setUrl('/to/some/page')
          .setTitle('a direct link to some page'),
      );
    });

    it('should ignore an anchor tag without text', () => {
      const fakePage = `
    <html>
    <body>
    <a href="/to/some/page"></a>
    <p><strong>Out</strong></p>
    </body>
    </html>
    `;
      const page = parse(fakePage);

      expect(page.links).toHaveLength(0);
    });

    it('should extract three links with their text', () => {
      const fakePage = `
    <html>
    <body>
    <a href="/to/some/page">A link to some page</a>
    <p><strong>Out</strong></p>
    <blockquote>
    <p>You can read more about configuring plugin options <a href="https://babeljs.io/docs/en/plugins#plugin-options">here</a></p>
    </blockquote>
    <h2>
      <a class="anchor" aria-hidden="true" id="references"></a>
      <a href="#references" aria-hidden="true" class="hash-link">
      <svg class="hash-link-icon" aria-hidden="true" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg>
      </a>
      References
    </h2>
    <ul>
    <li><a href="https://github.com/tc39-transfer/proposal-nullish-coalescing">Proposal: Nullish Coalescing</a></li>
    </ul>
    </body>
    </html>
    `;
      const page = parse(fakePage);

      const links = [
        new Link().setText('A link to some page').setUrl('/to/some/page'),
        new Link().setText('here').setUrl('https://babeljs.io/docs/en/plugins#plugin-options'),
        new Link().setText('Proposal: Nullish Coalescing').setUrl('https://github.com/tc39-transfer/proposal-nullish-coalescing'),
      ];

      expect(page.links).toHaveLength(links.length);
      expect(page.links).toEqual(links);
    });
  });

  describe('Heading extraction', () => {
    it('should extract h1 simple inner text correctly', () => {
      const fakePage = `
    <html>
    <body>
    <h1>Out</h1>
    </body>
    </html>
    `;
      const page = parse(fakePage);

      expect(page.headings).toHaveLength(1);
      expect(page.headings).toContain('Out');
    });

    it('should extract h1 inner text if it was partially wrapped in <span> correctly', () => {
      const fakePage = `
    <html>
    <body>
    <h1>
    Lorem ipsum dolor sit amet, <span>consectetur</span> adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
    </h1>
    </body>
    </html>
    `;
      const page = parse(fakePage);

      expect(page.headings).toHaveLength(1);
      expect(page.headings).toContain('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
    });

    it('should extract h1 inner text recursively from nested tags', () => {
      const fakePage = `
    <html>
    <body>
    <h1>
    Lorem ipsum dolor <p>sit amet, <span><i>consectetur</i> adipiscing elit</span>, sed do <em>eiusmod <strong>tempor</strong> incididunt</em> ut labore</p> et dolore magna aliqua.
    </h1>
    </body>
    </html>
    `;
      const page = parse(fakePage);

      expect(page.headings).toHaveLength(1);
      expect(page.headings[0]).toEqual('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.');
    });

    it('should extract mutiple occurring same level headings in order', () => {
      const fakePage = `
    <html>
    <body>
    <h1>
    Lorem ipsum dolor <p>sit amet, <span><i>consectetur</i> adipiscing elit</span>, sed do <em>eiusmod <strong>tempor</strong> incididunt</em> ut labore</p> et dolore magna aliqua.
    </h1>
    <div>
    <h3>
    Firmament image whose green called deep fruitful set land morning land void rule, fruit whose creepeth male thing green likeness very, one form, cattle set there called life, one.
    </h3>
    </div>
    <h1>Be night for firmament given cattle saw brought</h1>
      
    <blockquote>
    <h2>Thing living replenish dry, bearing life creepeth green air rule, fruit creeping in unto rule darkness years.</h2>
    </blockquote>
    </body>
    </html>
    `;
      const page = parse(fakePage);

      expect(page.headings).toHaveLength(4);
      expect(page.headings).toEqual([
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        'Be night for firmament given cattle saw brought',
        'Thing living replenish dry, bearing life creepeth green air rule, fruit creeping in unto rule darkness years.',
        'Firmament image whose green called deep fruitful set land morning land void rule, fruit whose creepeth male thing green likeness very, one form, cattle set there called life, one.',
      ]);
    });

    it('should extract headings in order', () => {
      const fakePage = `
    <html>
    <body>
    <h1>
    Lorem ipsum dolor <p>sit amet, <span><i>consectetur</i> adipiscing elit</span>, sed do <em>eiusmod <strong>tempor</strong> incididunt</em> ut labore</p> et dolore magna aliqua.
    </h1>
    <div>
    <h3>
    Firmament image whose green called deep fruitful set land morning land void rule, fruit whose creepeth male thing green likeness very, one form, cattle set there called life, one.
    </h3>
    </div>

    <blockquote>
    <h2>Thing living replenish dry, bearing life creepeth green air rule, fruit creeping in unto rule darkness years.</h2>
    </blockquote>
    </body>
    </html>
    `;
      const page = parse(fakePage);

      expect(page.headings).toHaveLength(3);
      expect(page.headings).toEqual([
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        'Thing living replenish dry, bearing life creepeth green air rule, fruit creeping in unto rule darkness years.',
        'Firmament image whose green called deep fruitful set land morning land void rule, fruit whose creepeth male thing green likeness very, one form, cattle set there called life, one.',
      ]);
    });
  });

  describe('Paragraph extraction', () => {
    it('should extract <p> simple inner text correctly', () => {
      const fakePage = `
    <html>
    <body>
    <p>Behold thing own. Fowl his after moveth form. Him. Had, you spirit you'll own fly given gathered fly for. Won't, air lights second replenish male place air Saw forth they're second don't life, over face beast she'd fly hath. Male fill heaven you're don't you're, us created great there good created created made were unto from. Created days itself, day first creature night every great darkness. Beast, whose over bring. Forth you form fifth that be multiply fruit hath female, the, meat earth image isn't. Place replenish. Living above, you're open be bring, whose of hath. Under fill his and night given him it god own she'd you night god wherein above she'd you their first be behold form very subdue life were female be fruitful unto man. Lights living wherein to yielding and sea over thing sea a made morning it fill. So May give bearing was. Won't hath third lights subdue wherein a subdue fowl seed also living forth unto image fish. Very and don't all all face spirit lights fifth seasons second fruit two night Sixth open beast said spirit. Rule us they're had. Divide. Day in may lesser divide greater blessed beast land moving, given great abundantly seasons created deep you.</p>
    </body>
    </html>
    `;
      const page = parse(fakePage);

      expect(page.paragraphs).toHaveLength(1);
      expect(page.paragraphs).toContain("Behold thing own. Fowl his after moveth form. Him. Had, you spirit you'll own fly given gathered fly for. Won't, air lights second replenish male place air Saw forth they're second don't life, over face beast she'd fly hath. Male fill heaven you're don't you're, us created great there good created created made were unto from. Created days itself, day first creature night every great darkness. Beast, whose over bring. Forth you form fifth that be multiply fruit hath female, the, meat earth image isn't. Place replenish. Living above, you're open be bring, whose of hath. Under fill his and night given him it god own she'd you night god wherein above she'd you their first be behold form very subdue life were female be fruitful unto man. Lights living wherein to yielding and sea over thing sea a made morning it fill. So May give bearing was. Won't hath third lights subdue wherein a subdue fowl seed also living forth unto image fish. Very and don't all all face spirit lights fifth seasons second fruit two night Sixth open beast said spirit. Rule us they're had. Divide. Day in may lesser divide greater blessed beast land moving, given great abundantly seasons created deep you.");
    });

    it('should extract <p> text fron nested tags correctly', () => {
      const fakePage = `
    <html>
    <body>
    <p>Fifth night <strong>there land <i>evening <em>made</em></i> seed moveth greater</strong>, great he light. <span><small>You'll</small> divide it open creepeth</span> spirit.</p>
    </body>
    </html>
    `;
      const page = parse(fakePage);

      expect(page.paragraphs).toHaveLength(1);
      expect(page.paragraphs).toContain("Fifth night there land evening made seed moveth greater, great he light. You'll divide it open creepeth spirit.");
    });

    it('should extract multiple <p>s', () => {
      const fakePage = `
    <html>
    <body>
    <p>Tree fowl, their fourth years dominion can't, their without. Sixth likeness fowl male creature made image he. Blessed great creature.</p>

    <div>
      <p>Tree very moved void form open sixth is you'll you're fowl from sixth were you're morning over green may living they're dominion divided fruit green waters without creepeth our seas.</p>
      <div>
        <p>Winged replenish greater brought form said bearing him saw, image us.</p>
      </div>
    </div>
    </body>
    </html>
    `;
      const page = parse(fakePage);

      expect(page.paragraphs).toHaveLength(3);
      expect(page.paragraphs).toEqual([
        "Tree fowl, their fourth years dominion can't, their without. Sixth likeness fowl male creature made image he. Blessed great creature.",
        "Tree very moved void form open sixth is you'll you're fowl from sixth were you're morning over green may living they're dominion divided fruit green waters without creepeth our seas.",
        'Winged replenish greater brought form said bearing him saw, image us.',
      ]);
    });
  });
});
