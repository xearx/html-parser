const startup = require('./startup');


function main() {
  return startup();
}

if (require.main === module) {
  main();
}
