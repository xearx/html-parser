/* eslint-disable global-require */
const Async = require('async');
const os = require('os');
const path = require('path');
const grpcServer = require('./grpc/server');

module.exports = function startup() {
  return Async.waterfall([
    async function configDotenv() {
      require('dotenv').config();
    },
    async function resolveConfig() {
      require('config');
    },
    async function instantiateLogger() {
      return {
        logger: require('./logger'),
      };
    },
    async function bindParserScript(passed) {
      require('./parser/parser');
      return passed;
    },
    async function initializeWorkerPool({ logger }) {
      logger.trace('instantiating worker pool...');
      const WorkerPool = require('./worker_pool');
      const parserScriptPath = path.resolve(__dirname, 'worker_pool', 'task_processor.js');
      const workerPool = new WorkerPool(os.cpus().length, parserScriptPath);
      logger.trace('worker pool instantiated.');
      return workerPool;
    },
    async function startGrpcServer(workerPool) {
      return grpcServer.connect({ workerPool });
    },
  ]);
};
