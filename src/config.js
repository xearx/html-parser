const config = require('config');

module.exports = {
  grpc: {
    proto: {
      relative_path: config.get('grpc.proto.relative_path'),
    },
    server: {
      host: config.get('grpc.server.host'),
      port: parseInt(config.get('grpc.server.port'), 10),
    },
  },
  logger: {
    level: config.get('logger.level').toString(),
  },
};
