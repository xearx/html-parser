const GrpcHandlers = require('./handlers');

module.exports = function resolveHandlers(workerPool) {
  return new GrpcHandlers(workerPool);
};
