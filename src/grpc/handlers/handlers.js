const Async = require('async');
const { serializePage } = require('../serializer');

module.exports = class GrpcHandlers {
  /**
   * @param {import('../../worker_pool')} workerPool
   */
  constructor(workerPool) {
    this.workerPool = workerPool;
  }

  ParsePages({ request }, reply) {
    Async.mapSeries(
      request.raw_pages,
      (rawPage, cb) => {
        this.workerPool.runTask({ page: rawPage.raw_content }, (error, parsedPage) => {
          if (error) {
            cb(undefined, undefined);
            return;
          }
          cb(undefined, serializePage(rawPage.url, parsedPage));
        });
      },
      (error, result) => {
        reply(error, { parsed_pages: result });
      },
    );
  }
};
