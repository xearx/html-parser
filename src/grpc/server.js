const grpc = require('grpc');
const config = require('../config');
const logger = require('../logger');
const resolvePackageDefinition = require('./protobuf');
const resolveHandlers = require('./handlers');


const Server = new grpc.Server();

function connect({ workerPool }) {
  logger.trace('resolving parser package definition...');
  const packageDefinition = resolvePackageDefinition();
  logger.trace('parser package definition resolved.');
  logger.trace('registering proto definitions to server...');
  Server.addService(packageDefinition.HTMLParser.service, resolveHandlers(workerPool));
  logger.trace('proto definitions registered to server.');
  const { host, port } = config.grpc.server;
  logger.trace({ host, port }, 'binding server to address...');
  Server.bind(`${host}:${port}`, grpc.ServerCredentials.createInsecure());
  logger.trace('server bound to address.');
  logger.trace('starting server...');
  Server.start();
  logger.trace('server started. listening for requests...');
  return Server;
}

function disconnect() {
  logger.trace('gracefully shutting down server...');
  Server.tryShutdown(() => {
    logger.trace('server shut down.');
  });
}

module.exports = {
  connect,
  disconnect,
};

if (require.main === module) {
  connect();
}
