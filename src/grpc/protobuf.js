const protoLoader = require('@grpc/proto-loader');
const path = require('path');
const grpc = require('grpc');
const config = require('../config');

module.exports = function resolvePackageDefinition() {
  const PROTO_PATH = path.resolve(config.grpc.proto.relative_path, 'parser_html.proto');

  // Suggested options for similarity to existing grpc.load behavior
  const packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {
      keepCase: true,
      longs: String,
      enums: String,
      defaults: true,
      oneofs: true,
    },
  );

  return grpc.loadPackageDefinition(packageDefinition);
};
