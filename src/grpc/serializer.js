/**
 * Serializes page model instance to grpc Page message.
 *
 * @param {string} url url of the page
 * @param {import('../models/page')} page page to serialize
 * @returns {object} grpc Page message type object
 */
function serializePage(url, page) {
  return {
    url,
    title: page.title,
    links: page.links,
    paragraphs: page.paragraphs,
    headings: page.headings,
  };
}

module.exports = {
  serializePage,
};
