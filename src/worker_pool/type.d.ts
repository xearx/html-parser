import Page from '../models/page';

export interface WorkerTask {
  page: string
}

export type WorketTaskCallback = (error: Error, result: Page) => void;
