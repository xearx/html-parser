const { EventEmitter } = require('events');
const { Worker } = require('worker_threads');
const WorkerPoolTaskInfo = require('./task_info');


const TASK_INFO = Symbol('TASK_INFO');
const WORKER_FREED_EVENT = Symbol('WORKER_FREED_EVENT');


class WorkerPool extends EventEmitter {
  /**
   * @param {number} numThreads - number of threads initially created.
   * @param {string} scriptPath - path to script file to get executed by workers.
   */
  constructor(numThreads, scriptPath) {
    super();
    this.setMaxListeners(5000);
    this.numThreads = numThreads;
    this.workers = [];
    this.freeWorkers = [];
    this.scriptPath = scriptPath;

    for (let i = 0; i < numThreads; i += 1) this.addNewWorker();
  }

  addNewWorker() {
    const worker = new Worker(this.scriptPath);
    worker.on('message', (result) => {
      // In case of success: Call the callback that was passed to `runTask`,
      // remove the `TaskInfo` associated with the Worker, and mark it as free
      // again.
      worker[TASK_INFO].done(null, result);
      worker[TASK_INFO] = null;
      this.freeWorkers.push(worker);
      this.emit(WORKER_FREED_EVENT);
    });
    worker.on('error', (err) => {
      // In case of an uncaught exception: Call the callback that was passed to
      // `runTask` with the error.
      if (worker[TASK_INFO]) worker[TASK_INFO].done(err, null);
      else this.emit('error', err);
      // Remove the worker from the list and start a new Worker to replace the
      // current one.
      this.workers.splice(this.workers.indexOf(worker), 1);
      this.addNewWorker();
    });
    this.workers.push(worker);
    this.freeWorkers.push(worker);
    this.emit(WORKER_FREED_EVENT);
  }

  /**
   * Runs the task and calls passed callback function with (error, result) once any of them happens.
   *
   * @param {import('./type').WorkerTask} task actual page html string.
   * @param {import('./type').WorketTaskCallback} callback
   * callback function gets executed once task is done.
   */
  runTask(task, callback) {
    if (this.freeWorkers.length === 0) {
      // No free threads, wait until a worker thread becomes free.
      this.once(WORKER_FREED_EVENT, () => this.runTask(task, callback));
      return;
    }

    const worker = this.freeWorkers.pop();
    worker[TASK_INFO] = new WorkerPoolTaskInfo(callback);
    worker.postMessage(task);
  }

  close() {
    for (const worker of this.workers) worker.terminate();
  }
}

module.exports = WorkerPool;
