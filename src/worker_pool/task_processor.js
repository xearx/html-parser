const { parentPort } = require('worker_threads');
const { parse } = require('../parser/parser');

parentPort.on('message', (task) => {
  const page = parse(task.page);
  parentPort.postMessage(page);
});
