module.exports = {
  env: {
    commonjs: true,
    es2020: true,
    node: true,
    jest: true,
  },
  parser: 'babel-eslint',
  extends: [
    'airbnb-base',
  ],
  plugins: [
    'jest',
    'babel',
  ],
  parserOptions: {
    ecmaVersion: 11,
    ecmaFeatures: {
      impliedStrict: true,
    },
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    strict: [
      'error',
      'global',
    ],
    'no-await-in-loop': [
      'off',
    ],
    'no-restricted-syntax': [
      'off',
    ],
    'no-multiple-empty-lines': [
      'error',
      {
        max: 2,
        maxEOF: 0,
        maxBOF: 0,
      },
    ],
  },
};
